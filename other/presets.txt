Guitar Loop - Setting 1: 
	Gain: 		0 dB
	Threshold: 	-
	Hysteresis: 0.37
	Inertia: 	0.7
	
	
Guitar Loop - Setting 2: 
	Gain: 		0 dB
	Threshold: 	-
	Hysteresis: 0.18
	Inertia: 	0.825
	
	
Drum Loop - Setting 1:
	Gain: 		0 dB
	Threshold:	-
	Hysteresis: 0.08
	Inertia:	0.78
	
Drum Loop - Setting 1:
	Gain: 		1 dB
	Threshold:	-
	Hysteresis: 0.08
	Inertia:	0.89
	
I suggest Trying all settings on both instrument loops 
and tweaking the 'Hysteresis' and 'Inertia' parameters live 
for intersting effects.