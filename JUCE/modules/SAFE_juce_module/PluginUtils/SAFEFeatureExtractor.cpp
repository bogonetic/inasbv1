//==========================================================================
//      Constructor and Destructor
//==========================================================================
SAFEFeatureExtractor::SAFEFeatureExtractor()
{
    initialised = false;
}

SAFEFeatureExtractor::~SAFEFeatureExtractor()
{
    if (initialised)
    {
        for (int n = 0; n < 13; ++n)
        {
            delete[] melFilters.filters [n];
        }

        delete[] melFilters.filters;
    }
}

void SAFEFeatureExtractor::initialise (int numAnalysisFramesInit, int analysisFrameLengthInit, double sampleRate)
{
    numAnalysisFrames = numAnalysisFramesInit;
    analysisFrameLength = analysisFrameLengthInit;
    fs = sampleRate;

    means.allocate (numAnalysisFrames, true);
    variances.allocate (numAnalysisFrames, true);
    standardDeviations.allocate (numAnalysisFrames, true);
    rmsAmplitudes.allocate (numAnalysisFrames, true);
    zeroCrossingRates.allocate (numAnalysisFrames, true);
    
    spectrum.allocate (analysisFrameLength, true);
    spectralCentroids.allocate (numAnalysisFrames, true);
    spectralVariances.allocate (numAnalysisFrames, true);
    spectralStandardDeviations.allocate (numAnalysisFrames, true);
    spectralSkewnesss.allocate (numAnalysisFrames, true);
    spectralKurtosiss.allocate (numAnalysisFrames, true);
    irregularityJs.allocate (numAnalysisFrames, true);
    irregularityKs.allocate (numAnalysisFrames, true);
    fundamentals.allocate (numAnalysisFrames, true);
    smoothnesss.allocate (numAnalysisFrames, true);
    rolloffs.allocate (numAnalysisFrames, true);
    flatnesss.allocate (numAnalysisFrames, true);
    tonalities.allocate (numAnalysisFrames, true);
    crests.allocate (numAnalysisFrames, true);
    spectralSlopes.allocate (numAnalysisFrames, true);

    peakSpectrum.allocate (analysisFrameLength, true);
    peakSpectralCentroids.allocate (numAnalysisFrames, true);
    peakSpectralVariances.allocate (numAnalysisFrames, true);
    peakSpectralStandardDeviations.allocate (numAnalysisFrames, true);
    peakSpectralSkewnesss.allocate (numAnalysisFrames, true);
    peakSpectralKurtosiss.allocate (numAnalysisFrames, true);
    peakIrregularityJs.allocate (numAnalysisFrames, true);
    peakIrregularityKs.allocate (numAnalysisFrames, true);
    peakTristimulus1s.allocate (numAnalysisFrames, true);
    peakTristimulus2s.allocate (numAnalysisFrames, true);
    peakTristimulus3s.allocate (numAnalysisFrames, true);
    inharmonicities.allocate (numAnalysisFrames, true);

    harmonicSpectrum.allocate (analysisFrameLength, true);
    harmonicSpectralCentroids.allocate (numAnalysisFrames, true);
    harmonicSpectralVariances.allocate (numAnalysisFrames, true);
    harmonicSpectralStandardDeviations.allocate (numAnalysisFrames, true);
    harmonicSpectralSkewnesss.allocate (numAnalysisFrames, true);
    harmonicSpectralKurtosiss.allocate (numAnalysisFrames, true);
    harmonicIrregularityJs.allocate (numAnalysisFrames, true);
    harmonicIrregularityKs.allocate (numAnalysisFrames, true);
    harmonicTristimulus1s.allocate (numAnalysisFrames, true);
    harmonicTristimulus2s.allocate (numAnalysisFrames, true);
    harmonicTristimulus3s.allocate (numAnalysisFrames, true);
    noisinesss.allocate (numAnalysisFrames, true);
    parityRatios.allocate (numAnalysisFrames, true);

    barkBandLimits.allocate (26, true);
    xtract_init_bark (analysisFrameLength, fs, barkBandLimits);
    barkCoefficients.allocate (25, true);
    loudnesss.allocate (numAnalysisFrames, true);
    sharpnesss.allocate (numAnalysisFrames, true);

    melFilters.n_filters = 13;
    melFilters.filters = new double* [13];
    for (int n = 0; n < 13; ++n)
    {
        melFilters.filters [n] = new double [analysisFrameLength];
    }
    mfccs.allocate (13, true);
    xtract_init_mfcc (analysisFrameLength / 2, fs / 2, XTRACT_EQUAL_GAIN, 20, 20000, melFilters.n_filters, melFilters.filters);
    mfccMeans.allocate (numAnalysisFrames, true);
    mfccVariances.allocate (numAnalysisFrames, true);
    mfccStandardDeviations.allocate (numAnalysisFrames, true);
    mfccSkewnesss.allocate (numAnalysisFrames, true);
    mfccKurtosiss.allocate (numAnalysisFrames, true);

    initialised = true;
}

void SAFEFeatureExtractor::getAllFeatures (double* sampleData, int numSamples, int frameNum)
{
    // time domain features
    xtract_mean (sampleData, numSamples, NULL, means + frameNum);
    xtract_variance (sampleData, numSamples, means + frameNum, variances + frameNum);
    xtract_standard_deviation (sampleData, numSamples, variances + frameNum, standardDeviations + frameNum);
    xtract_rms_amplitude (sampleData, numSamples, NULL, rmsAmplitudes + frameNum);
    xtract_zcr (sampleData, numSamples, NULL, zeroCrossingRates + frameNum);

    // frequency domain features
    double argumentArray [4] = {fs / numSamples, XTRACT_MAGNITUDE_SPECTRUM, 0, 0};
    xtract_spectrum (sampleData, numSamples, argumentArray, spectrum);
    xtract_spectral_centroid (spectrum, numSamples, NULL, spectralCentroids + frameNum);
    xtract_spectral_variance (spectrum, numSamples, spectralCentroids + frameNum, spectralVariances + frameNum);
    xtract_spectral_standard_deviation (spectrum, numSamples, spectralVariances + frameNum, spectralStandardDeviations + frameNum);
    argumentArray [0] = spectralCentroids [frameNum];
    argumentArray [1] = spectralStandardDeviations [frameNum];
    xtract_spectral_skewness (spectrum, numSamples, argumentArray, spectralSkewnesss + frameNum);
    xtract_spectral_kurtosis (spectrum, numSamples, argumentArray, spectralKurtosiss + frameNum);
    xtract_irregularity_j (spectrum, numSamples / 2, NULL, irregularityJs + frameNum);
    xtract_irregularity_k (spectrum, numSamples / 2, NULL, irregularityKs + frameNum);
    xtract_hps (spectrum, numSamples, NULL, fundamentals + frameNum);
    xtract_smoothness (spectrum, numSamples / 2, NULL, smoothnesss + frameNum);
    argumentArray [0] = fs / numSamples;
    argumentArray [1] = 45;
    xtract_rolloff (spectrum, numSamples / 2, argumentArray, rolloffs + frameNum);
    xtract_flatness (spectrum, numSamples / 2, NULL, flatnesss + frameNum);
    double logFlatness;
    xtract_flatness_db (NULL, 0, flatnesss + frameNum, &logFlatness);
    xtract_tonality (NULL, 0, &logFlatness, tonalities + frameNum);
    xtract_highest_value (spectrum, numSamples / 2, NULL, argumentArray);
    xtract_mean (spectrum, numSamples / 2, NULL, argumentArray + 1);
    xtract_crest (NULL, 0, argumentArray, crests + frameNum);
    xtract_spectral_slope (spectrum, numSamples, NULL, spectralSlopes + frameNum);

    // peak spectrum features
    argumentArray [0] = fs / numSamples;
    argumentArray [1] = 10;
    xtract_peak_spectrum (spectrum, numSamples / 2, argumentArray, peakSpectrum);
    xtract_spectral_centroid (peakSpectrum, numSamples, NULL, peakSpectralCentroids + frameNum);
    xtract_spectral_variance (peakSpectrum, numSamples, peakSpectralCentroids + frameNum, peakSpectralVariances + frameNum);
    xtract_spectral_standard_deviation (peakSpectrum, numSamples, peakSpectralVariances + frameNum, peakSpectralStandardDeviations + frameNum);
    argumentArray [0] = peakSpectralCentroids [frameNum];
    argumentArray [1] = peakSpectralStandardDeviations [frameNum];
    xtract_spectral_skewness (peakSpectrum, numSamples, argumentArray, peakSpectralSkewnesss + frameNum);
    xtract_spectral_kurtosis (peakSpectrum, numSamples, argumentArray, peakSpectralKurtosiss + frameNum);
    xtract_irregularity_j (peakSpectrum, numSamples / 2, NULL, peakIrregularityJs + frameNum);
    xtract_irregularity_k (peakSpectrum, numSamples / 2, NULL, peakIrregularityKs + frameNum);
    xtract_tristimulus_1 (peakSpectrum, numSamples, fundamentals + frameNum, peakTristimulus1s + frameNum);
    xtract_tristimulus_2 (peakSpectrum, numSamples, fundamentals + frameNum, peakTristimulus2s + frameNum);
    xtract_tristimulus_3 (peakSpectrum, numSamples, fundamentals + frameNum, peakTristimulus3s + frameNum);
    xtract_spectral_inharmonicity (peakSpectrum, numSamples, fundamentals + frameNum, inharmonicities + frameNum);

    // harmonic spectrum features
    argumentArray [0] = fundamentals [frameNum];
    argumentArray [1] = 0.2;
    xtract_harmonic_spectrum (peakSpectrum, numSamples, argumentArray, harmonicSpectrum);
    xtract_spectral_centroid (harmonicSpectrum, numSamples, NULL, harmonicSpectralCentroids + frameNum);
    xtract_spectral_variance (harmonicSpectrum, numSamples, harmonicSpectralCentroids + frameNum, harmonicSpectralVariances + frameNum);
    xtract_spectral_standard_deviation (harmonicSpectrum, numSamples, harmonicSpectralVariances + frameNum, harmonicSpectralStandardDeviations + frameNum);
    argumentArray [0] = harmonicSpectralCentroids [frameNum];
    argumentArray [1] = harmonicSpectralStandardDeviations [frameNum];
    xtract_spectral_skewness (harmonicSpectrum, numSamples, argumentArray, harmonicSpectralSkewnesss + frameNum);
    xtract_spectral_kurtosis (harmonicSpectrum, numSamples, argumentArray, harmonicSpectralKurtosiss + frameNum);
    xtract_irregularity_j (harmonicSpectrum, numSamples / 2, NULL, harmonicIrregularityJs + frameNum);
    xtract_irregularity_k (harmonicSpectrum, numSamples / 2, NULL, harmonicIrregularityKs + frameNum);
    xtract_tristimulus_1 (harmonicSpectrum, numSamples, fundamentals + frameNum, harmonicTristimulus1s + frameNum);
    xtract_tristimulus_2 (harmonicSpectrum, numSamples, fundamentals + frameNum, harmonicTristimulus2s + frameNum);
    xtract_tristimulus_3 (harmonicSpectrum, numSamples, fundamentals + frameNum, harmonicTristimulus3s + frameNum);
    double numHarmonics, numPartials;
    xtract_nonzero_count (harmonicSpectrum, numSamples / 2, NULL, &numHarmonics);
    xtract_nonzero_count (peakSpectrum, numSamples / 2, NULL, &numPartials);
    argumentArray [0] = numHarmonics;
    argumentArray [1] = numPartials;
    xtract_noisiness (NULL, 0, argumentArray, noisinesss + frameNum);
    xtract_odd_even_ratio (harmonicSpectrum, numSamples, fundamentals + frameNum, parityRatios + frameNum);

    // bark features
    xtract_bark_coefficients (spectrum, numSamples / 2, barkBandLimits, barkCoefficients);
    xtract_loudness (barkCoefficients, 25, NULL, loudnesss + frameNum);
    xtract_sharpness (barkCoefficients, 25, NULL, sharpnesss + frameNum);

    // mfcc features
    xtract_mfcc (spectrum, numSamples / 2, &melFilters, mfccs);
    xtract_mean (mfccs, 13, NULL, mfccMeans + frameNum);
    xtract_variance (mfccs, 13, mfccMeans + frameNum, mfccVariances + frameNum);
    xtract_standard_deviation (mfccs, 13, mfccVariances + frameNum, mfccStandardDeviations + frameNum);
    argumentArray [0] = mfccMeans [frameNum];
    argumentArray [1] = mfccStandardDeviations [frameNum];
    xtract_skewness (mfccs, 13, argumentArray, mfccSkewnesss + frameNum);
    xtract_kurtosis (mfccs, 13, argumentArray, mfccKurtosiss + frameNum);
}

void SAFEFeatureExtractor::addToXml (XmlElement* parentElement)
{
    for (int frameNum = 0; frameNum < numAnalysisFrames; ++frameNum)
    {
        String frameName = String ("Frame") + String (frameNum);

        XmlElement* frameElement = parentElement->getChildByName (frameName);

        if (! frameElement)
        {
            frameElement = parentElement->createNewChildElement (frameName);
        }

        frameElement->setAttribute ("Mean", means [frameNum]);
        frameElement->setAttribute ("Variance", variances [frameNum]);
        frameElement->setAttribute ("Standard_Deviation", standardDeviations [frameNum]);
        frameElement->setAttribute ("RMS_Amplitude", rmsAmplitudes [frameNum]);
        frameElement->setAttribute ("Zero_Crossing_Rate", zeroCrossingRates [frameNum]);

        frameElement->setAttribute ("Spectral_Centroid", spectralCentroids [frameNum]);
        frameElement->setAttribute ("Spectral_Variance", spectralVariances [frameNum]);
        frameElement->setAttribute ("Spectral_Standard_Deviation", spectralStandardDeviations [frameNum]);
        frameElement->setAttribute ("Spectral_Skewness", spectralSkewnesss [frameNum]);
        frameElement->setAttribute ("Spectral_Kurtosis", spectralKurtosiss [frameNum]);
        frameElement->setAttribute ("Irregularity_J", irregularityJs [frameNum]);
        frameElement->setAttribute ("Irregularity_K", irregularityKs [frameNum]);
        frameElement->setAttribute ("Fundamental", fundamentals [frameNum]);
        frameElement->setAttribute ("Smoothness", smoothnesss [frameNum]);
        frameElement->setAttribute ("Spectral_Roll_Off", rolloffs [frameNum]);
        frameElement->setAttribute ("Spectral_Flatness", flatnesss [frameNum]);
        frameElement->setAttribute ("Tonality", tonalities [frameNum]);
        frameElement->setAttribute ("Spectral_Crest", crests [frameNum]);
        frameElement->setAttribute ("Spectral_Slope", spectralSlopes [frameNum]);

        frameElement->setAttribute ("Peak_Spectral_Centroid", peakSpectralCentroids [frameNum]);
        frameElement->setAttribute ("Peak_Spectral_Variance", peakSpectralVariances [frameNum]);
        frameElement->setAttribute ("Peak_Spectral_Standard_Deviation", peakSpectralStandardDeviations [frameNum]);
        frameElement->setAttribute ("Peak_Spectral_Skewness", peakSpectralSkewnesss [frameNum]);
        frameElement->setAttribute ("Peak_Spectral_Kurtosis", peakSpectralKurtosiss [frameNum]);
        frameElement->setAttribute ("Peak_Irregularity_J", peakIrregularityJs [frameNum]);
        frameElement->setAttribute ("Peak_Irregularity_K", peakIrregularityKs [frameNum]);
        frameElement->setAttribute ("Peak_Tristimulus_1", peakTristimulus1s [frameNum]);
        frameElement->setAttribute ("Peak_Tristimulus_2", peakTristimulus2s [frameNum]);
        frameElement->setAttribute ("Peak_Tristimulus_3", peakTristimulus3s [frameNum]);
        frameElement->setAttribute ("Inharmonicity", inharmonicities [frameNum]);

        frameElement->setAttribute ("Harmonic_Spectral_Centroid", harmonicSpectralCentroids [frameNum]);
        frameElement->setAttribute ("Harmonic_Spectral_Variance", harmonicSpectralVariances [frameNum]);
        frameElement->setAttribute ("Harmonic_Spectral_Standard_Deviation", harmonicSpectralStandardDeviations [frameNum]);
        frameElement->setAttribute ("Harmonic_Spectral_Skewness", harmonicSpectralSkewnesss [frameNum]);
        frameElement->setAttribute ("Harmonic_Spectral_Kurtosis", harmonicSpectralKurtosiss [frameNum]);
        frameElement->setAttribute ("Harmonic_Irregularity_J", harmonicIrregularityJs [frameNum]);
        frameElement->setAttribute ("Harmonic_Irregularity_K", harmonicIrregularityKs [frameNum]);
        frameElement->setAttribute ("Harmonic_Tristimulus_1", harmonicTristimulus1s [frameNum]);
        frameElement->setAttribute ("Harmonic_Tristimulus_2", harmonicTristimulus2s [frameNum]);
        frameElement->setAttribute ("Harmonic_Tristimulus_3", harmonicTristimulus3s [frameNum]);
        frameElement->setAttribute ("Noisiness", noisinesss [frameNum]);
        frameElement->setAttribute ("Parity_Ratio", parityRatios [frameNum]);

        frameElement->setAttribute ("Loudness", loudnesss [frameNum]);
        frameElement->setAttribute ("Sharpness", sharpnesss [frameNum]);

        frameElement->setAttribute ("MFCC_Mean", mfccMeans [frameNum]);
        frameElement->setAttribute ("MFCC_Variance", mfccVariances [frameNum]);
        frameElement->setAttribute ("MFCC_Standard_Deviation", mfccStandardDeviations [frameNum]);
        frameElement->setAttribute ("MFCC_Skewness", mfccSkewnesss [frameNum]);
        frameElement->setAttribute ("MFCC_Kurtosis", mfccKurtosiss [frameNum]);
    }
}
