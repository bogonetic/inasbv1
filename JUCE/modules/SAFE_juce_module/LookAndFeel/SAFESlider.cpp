SAFESlider::SAFESlider()
    : label ("", ""),
      slider (Slider::RotaryHorizontalVerticalDrag, Slider::NoTextBox)
{
    setSize (80, 80);

    addAndMakeVisible (&slider);
    slider.setBounds (0, 0, 80, 80);

    addAndMakeVisible (&label);
    label.setBounds (0, 0, 80, 80);
    label.setInterceptsMouseClicks (false, false);
    label.setJustificationType (Justification::centred);
}

SAFESlider::~SAFESlider()
{
}

void SAFESlider::setColour (int colourId, Colour newColour)
{
    slider.setColour (colourId, newColour);
}

void SAFESlider::setValue (double newValue, NotificationType notification)
{
    slider.setValue (newValue, notification);
}

double SAFESlider::getValue()
{
    return slider.getValue();
}

void SAFESlider::setRange (double newMinimum, double newMaximum, double newInterval)
{
    slider.setRange (newMinimum, newMaximum, newInterval);
}

void SAFESlider::setSkewFactor (double factor)
{
    slider.setSkewFactor (factor);
}

void SAFESlider::setTextValueSuffix (const String &suffix)
{
    slider.setTextValueSuffix (suffix);
}

void SAFESlider::addListener (Slider::Listener* listener)
{
    slider.addListener (listener);
}

const Slider* SAFESlider::getSliderPointer() const
{
    return &slider;
}

void SAFESlider::setText (String newText)
{
    label.setText (newText, dontSendNotification);
}

void SAFESlider::resized()
{
    int width = getWidth();
    int height = getHeight();

    slider.setBounds (0, 0, width, height);
    label.setBounds (0, 0, width, height);
}
