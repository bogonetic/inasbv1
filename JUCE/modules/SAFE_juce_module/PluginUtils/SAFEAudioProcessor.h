#ifndef __SAFEAUDIOPROCESSOR__
#define __SAFEAUDIOPROCESSOR__

class SAFEAudioProcessor : public AudioProcessor
{
public:
    //==========================================================================
    //      Constructor and Destructor
    //==========================================================================
    SAFEAudioProcessor();
    virtual ~SAFEAudioProcessor();
    
    //==========================================================================
    //      Parameter Info Methods
    //==========================================================================
    const String getName() const;

    int getNumParameters();

    float getParameter (int index);
    void setParameter (int index, float newValue);
    
    float getScaledParameter (int index);
    void setScaledParameter (int index, float newValue);
    void setScaledParameterNotifyingHost (int index, float newValue);
    
    virtual void parameterUpdateCalculations (int index) {};

    const String getParameterName (int index) override;
    const String getParameterText (int index);
    
    const OwnedArray <SAFEParameter>& getParameterArray();

    //==========================================================================
    //      Other Plugin Info
    //==========================================================================
    const String getInputChannelName (int channelIndex) const;
    const String getOutputChannelName (int channelIndex) const;
    bool isInputChannelStereoPair (int index) const;
    bool isOutputChannelStereoPair (int index) const;

    bool acceptsMidi() const;
    bool producesMidi() const;
    bool silenceInProducesSilenceOut() const;
    double getTailLengthSeconds() const;

    //==========================================================================
    //      Program Stuff
    //==========================================================================
    int getNumPrograms();
    int getCurrentProgram();
    void setCurrentProgram (int index);
    const String getProgramName (int index);
    void changeProgramName (int index, const String& newName);

    //==========================================================================
    //      Saving and Loading Patches
    //==========================================================================
    void getStateInformation (MemoryBlock& destData);
    void setStateInformation (const void* data, int sizeInBytes);

    //==========================================================================
    //      Semantic Data Parsing
    //==========================================================================
    void initialiseSemanticDataFile();
    void saveSemanticData (String newDescriptors, SAFEMetaData& metaData);
    void loadSemanticData (String descriptor);

    //==========================================================================
    //      Process Block
    //==========================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock);
    virtual void pluginPreparation (double sampleRate, int samplesPerBlock) = 0;
    void processBlock (AudioSampleBuffer& buffer, MidiBuffer& midiMessages);
    virtual void pluginProcessing (AudioSampleBuffer& buffer, MidiBuffer& midiMessages) = 0;

    //==========================================================================
    //      Playing & Recording Info
    //==========================================================================
    bool isPlaying();
    void startRecording();
    bool isRecording();
    bool isReadyToSave();

protected:
    //==========================================================================
    //      Methods to Create New Parameters
    //==========================================================================
    void addParameter (String name, float& valueRef, float initialValue = 1, float minValue = 0, float maxValue = 1, String units = String::empty, float skewFactor = 1);

    //==========================================================================
    //      Buffer Playing Audio For Analysis
    //==========================================================================
    void analyseUnprocessedSamples (AudioSampleBuffer& buffer);
    void analyseProcessedSamples (AudioSampleBuffer& buffer);

    //==========================================================================
    //      Play Head Stuff
    //==========================================================================
    AudioPlayHead::CurrentPositionInfo playHead;
    void updatePlayHead();

    bool recording;
    bool readyToSave;

    //==========================================================================
    //      Multiple Channel Stuff
    //==========================================================================
    int numInputs;
    int numOutputs;

private:
    bool localRecording;

    OwnedArray <SAFEParameter> parameters;

    File semanticDataFile;
    ScopedPointer <XmlElement> semanticDataElement;

    static const int analysisTime = 5000;
    static const int analysisFrameLength = 4096;
    int numAnalysisFrames, currentUnprocessedAnalysisFrame, currentProcessedAnalysisFrame;
    OwnedArray <Array <double> > unprocessedBuffer, processedBuffer;
    int unprocessedTap, processedTap;

    OwnedArray <SAFEFeatureExtractor> unprocessedFeatureExtractors, processedFeatureExtractors;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SAFEAudioProcessor);
};

#endif  // __SAFEAUDIOPROCESSOR__
