#ifndef IMAGE_DIRECTORY
#define IMAGE_DIRECTORY File::getSpecialLocation (File::currentExecutableFile)
#endif

SAFEButton::SAFEButton(const String& buttonName)
    : Button (buttonName)
{
    currentMode = Record;

    File imageDirectory (IMAGE_DIRECTORY);

    File recordFile = imageDirectory.getChildFile ("Images/record.png");
    recordImage = ImageCache::getFromFile (recordFile);

    File recordMouseOverFile = imageDirectory.getChildFile ("Images/record_MO.png");
    recordMouseOverImage = ImageCache::getFromFile (recordMouseOverFile);

    File recordingFile = imageDirectory.getChildFile  ("Images/recording.png");
    recordingImage = ImageCache::getFromFile (recordingFile);

    File saveFile = imageDirectory.getChildFile  ("Images/save.png");
    saveImage = ImageCache::getFromFile (saveFile);
    
    File saveMouseOverFile = imageDirectory.getChildFile  ("Images/save_MO.png");
    saveMouseOverImage = ImageCache::getFromFile (saveMouseOverFile);

    File loadFile = imageDirectory.getChildFile  ("Images/load.png");
    loadImage = ImageCache::getFromFile (loadFile);
    
    File loadMouseOverFile = imageDirectory.getChildFile  ("Images/load_MO.png");
    loadMouseOverImage = ImageCache::getFromFile (loadMouseOverFile);

    File metaDataFile = imageDirectory.getChildFile  ("Images/metadata.png");
    metaDataImage = ImageCache::getFromFile (metaDataFile);

    File metaDataMouseOverFile = imageDirectory.getChildFile  ("Images/metadata_MO.png");
    metaDataMouseOverImage = ImageCache::getFromFile (metaDataMouseOverFile);
}

SAFEButton::~SAFEButton()
{
}

void SAFEButton::paintButton (Graphics& g, bool isMouseOverButton, bool isButtonDown)
{
    int width = getWidth();
    int height = getHeight();
    
    Image imageToDraw, mouseOverImageToDraw;

    switch (currentMode)
    {
        case Record:
            imageToDraw = recordImage;
            mouseOverImageToDraw = recordMouseOverImage;
            break;

        case Recording:
            imageToDraw = recordingImage;
            mouseOverImageToDraw = recordingImage;
            break;

        case Save:
            imageToDraw = saveImage;
            mouseOverImageToDraw = saveMouseOverImage;
            break;

        case Load:
            imageToDraw = loadImage;
            mouseOverImageToDraw = loadMouseOverImage;
            break;

        case MetaData:
            imageToDraw = metaDataImage;
            mouseOverImageToDraw = metaDataMouseOverImage;
            break;
    }

    int imageWidth = imageToDraw.getWidth();
    int mouseOverImageWidth = mouseOverImageToDraw.getWidth();

    int imageHeight = imageToDraw.getHeight();
    int mouseOverImageHeight = mouseOverImageToDraw.getHeight();

    if (isMouseOverButton)
    {
        g.drawImage (mouseOverImageToDraw, 0, 0, width, height, 0, 0, mouseOverImageWidth, mouseOverImageHeight);
    }
    else
    {
        g.drawImage (imageToDraw, 0, 0, width, height, 0, 0, imageWidth, imageHeight);
    }

}

void SAFEButton::setMode (ButtonMode newMode)
{
    currentMode = newMode;
    repaint();
}
