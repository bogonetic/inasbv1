#ifndef __SAFEBUTTON__
#define __SAFEBUTTON__

class SAFEButton : public Button
{
public:
    SAFEButton(const String& buttonName);
    ~SAFEButton();

    enum ButtonMode
    {
        Record,
        Recording,
        Save,
        Load,
        MetaData
    };

    void paintButton (Graphics& g, bool isMouseOverButton, bool isButtonDown);

    void setMode (ButtonMode newMode);

private:
    ButtonMode currentMode;

    Image recordImage, recordMouseOverImage; 
    Image recordingImage;
    Image saveImage, saveMouseOverImage;
    Image loadImage, loadMouseOverImage;
    Image metaDataImage, metaDataMouseOverImage;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SAFEButton)
};

#endif // __SAFEBUTTON__
