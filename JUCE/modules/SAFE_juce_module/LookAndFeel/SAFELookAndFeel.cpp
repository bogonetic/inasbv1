#ifndef IMAGE_DIRECTORY
#define IMAGE_DIRECTORY File::getSpecialLocation (File::currentExecutableFile)
#endif

SAFELookAndFeel::SAFELookAndFeel()
{
    File imageDirectory (IMAGE_DIRECTORY);

    File greenSliderFile = imageDirectory.getChildFile ("Images/knob_green.png");
    greenSliderImage = ImageCache::getFromFile (greenSliderFile);
    
    File redSliderFile = imageDirectory.getChildFile ("Images/knob_red.png");
    redSliderImage = ImageCache::getFromFile (redSliderFile);

    File yellowSliderFile = imageDirectory.getChildFile ("Images/knob_yellow.png");
    yellowSliderImage = ImageCache::getFromFile (yellowSliderFile);
}

SAFELookAndFeel::~SAFELookAndFeel()
{
}

void SAFELookAndFeel::drawRotarySlider (Graphics &g, int x, int y, int width, int height, float sliderPosProportional, float rotaryStartAngle, float rotaryEndAngle, Slider &slider)
{
    Image imageToDraw;
    Colour sliderColour = slider.findColour (Slider::rotarySliderFillColourId);

    if (sliderColour == Colours::green)
    {
        imageToDraw = greenSliderImage;
    }
    else if (sliderColour == Colours::red)
    {
        imageToDraw = redSliderImage;
    }
    else
    {
        imageToDraw = yellowSliderImage;
    }

    AffineTransform transform;

    int imageWidth = imageToDraw.getWidth();
    int imageHeight = imageToDraw.getHeight();
    
    float widthFactor = (float) width / imageWidth;
    float heightFactor = (float) height / imageHeight;

    float minFactor = 1;
    float xTranslate = 0;
    float yTranslate = 0;
    if (widthFactor >= heightFactor)
    {
        minFactor = heightFactor;
        xTranslate = (width - imageWidth * minFactor) / 2;
    }
    else
    {
        minFactor = widthFactor;
        yTranslate = (height - imageHeight * minFactor) / 2;
    }

    transform = transform.scaled (minFactor);
    transform = transform.translated (xTranslate, yTranslate);

    float sliderRange = 3.0f * float_Pi / 2.0f;
    float sliderAngle = sliderPosProportional * sliderRange - 3.0f * float_Pi / 4.0f;
    transform = transform.rotated (sliderAngle, width / 2, height / 2);
    
    g.drawImageTransformed (imageToDraw, transform);
}

void SAFELookAndFeel::fillTextEditorBackground (Graphics &g, int width, int height, TextEditor &textEditor)
{
    g.setColour (Colour (0xff999999));

    g.fillRect (0, 0, width, height);
}

void SAFELookAndFeel::drawTextEditorOutline (Graphics &g, int width, int height, TextEditor &textEditor)
{
    g.setColour (Colour (0xff000000));

    g.drawRect (0, 0, width, height, 1);
}
