#ifndef __SAFESLIDER__
#define __SAFESLIDER__

class SAFESlider : public Component
{
public:
    SAFESlider();
    ~SAFESlider();

    void setColour (int colourId, Colour newColour);
    void setValue (double newValue, NotificationType notification);
    double getValue();
    void setRange (double newMinimum, double newMaximum, double newInterval);
    void setSkewFactor (double factor);
    void setTextValueSuffix (const String &suffix);
    void addListener (Slider::Listener* listener);

    const Slider* getSliderPointer() const;

    void setText (String newText);

    void resized();

private:
    Slider slider;
    Label label;

};

#endif // __SAFESLIDER__
