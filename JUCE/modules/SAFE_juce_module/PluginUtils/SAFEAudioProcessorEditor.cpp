//==========================================================================
//      Constructor and Destructor
//==========================================================================
SAFEAudioProcessorEditor::SAFEAudioProcessorEditor (SAFEAudioProcessor* ownerFilter)
    : AudioProcessorEditor (ownerFilter),
      parameters (ownerFilter->getParameterArray()),
      recordButton ("Record"),
      loadButton ("Load"),
      metaDataButton ("Meta Data")
{
    lookAndFeel = new SAFELookAndFeel;
    setLookAndFeel (lookAndFeel);

    descriptorBox.setWantsKeyboardFocus (true);

    recordButton.addListener (this);

    loadButton.setMode (SAFEButton::Load);
    loadButton.addListener (this);

    numParameters = parameters.size();

    for (int n = 0; n < numParameters; ++n)
    {
        SAFESlider* currentSlider = sliders.add (new SAFESlider);
        
        SAFEParameter* currentParameter = parameters [n];
        float currentMinValue = currentParameter->getMinValue();
        float currentMaxValue = currentParameter->getMaxValue();
        float currentSkewFactor = currentParameter->getSkewFactor();
        String currentUnits = currentParameter->getUnits();

        currentSlider->setRange (currentMinValue, currentMaxValue, 0.01);
        currentSlider->setSkewFactor (currentSkewFactor);
        currentSlider->setTextValueSuffix (currentUnits);
        currentSlider->addListener (this);
    }

    metaDataXPos = -250;
    metaDataYPos = 0;
    addAndMakeVisible (&metaDataScreen);
    metaDataScreen.setBounds (metaDataXPos, metaDataYPos, 250, 100);
    metaDataScreen.setAlwaysOnTop (true);
    metaDataScreen.setEnabled (false);
    metaDataScreen.submitButton.addListener (this);

    metaDataButton.setMode (SAFEButton::MetaData);
    metaDataButton.addListener (this);

    startTimer (100);
}

SAFEAudioProcessorEditor::~SAFEAudioProcessorEditor()
{
}

//==========================================================================
//      User Interaction Listeners
//==========================================================================
void SAFEAudioProcessorEditor::buttonClicked (Button* button)
{
    SAFEAudioProcessor* ourProcessor = getProcessor();
    
    String descriptorBoxContent = descriptorBox.getText();
    
    SAFEMetaData metaData = metaDataScreen.getMetaData();
    
    if (button == &recordButton)
    {
        if (ourProcessor->isReadyToSave())
        {
            if (descriptorBoxContent.containsNonWhitespaceChars())
            {
                ourProcessor->saveSemanticData (descriptorBoxContent, metaData);
                recordButton.setMode (SAFEButton::Record);
            }
        }
        else if (ourProcessor->isPlaying())
        {
            ourProcessor->startRecording();
            recordButton.setMode (SAFEButton::Recording);
        }
    }
    else if (button == &loadButton)
    {
        if (descriptorBoxContent.containsNonWhitespaceChars())
        {
            ourProcessor->loadSemanticData (descriptorBoxContent);
        }
    }
    else if (button == &metaDataButton)
    {
        int numComponents = getNumChildComponents();

        for (int component = 0; component < numComponents; ++component)
        {
            getChildComponent (component)->setEnabled (false);
        }

        metaDataScreen.setEnabled (true);

        Rectangle <int> metaDataPosition = metaDataScreen.getBoundsInParent();
        metaDataPosition.setX (metaDataXPos);

        animator.animateComponent (&metaDataScreen, metaDataPosition, 0xff, 1000, false, 0, 0);
    }
    else if (button == &metaDataScreen.submitButton)
    {
        int numComponents = getNumChildComponents();

        for (int component = 0; component < numComponents; ++component)
        {
            getChildComponent (component)->setEnabled (true);
        }

        metaDataScreen.setEnabled (false);

        Rectangle <int> metaDataPosition = metaDataScreen.getBoundsInParent();
        metaDataPosition.setX (-250);

        animator.animateComponent (&metaDataScreen, metaDataPosition, 0xff, 1000, false, 0, 0);
    }
}

void SAFEAudioProcessorEditor::sliderValueChanged (Slider* slider)
{
    SAFEAudioProcessor* ourProcessor = getProcessor();

    for (int n = 0; n < numParameters; ++n)
    {
        if (slider == sliders [n]->getSliderPointer())
        {
            ourProcessor->setScaledParameterNotifyingHost (n, (float) sliders [n]->getValue());
        }
    }
}

//==========================================================================
//      GUI Update Timer
//==========================================================================
void SAFEAudioProcessorEditor::timerCallback()
{
    SAFEAudioProcessor* ourProcessor = getProcessor();

    if (ourProcessor->isReadyToSave())
    {
        recordButton.setMode (SAFEButton::Save);
    }

    for (int n = 0; n < numParameters; ++n)
    {
        sliders [n]->setValue (ourProcessor->getScaledParameter (n), dontSendNotification);
    }

}

//==========================================================================
//      Meta Data Screen Bits
//==========================================================================
void SAFEAudioProcessorEditor::setMetaDataScreenPosition (int x, int y)
{
    metaDataXPos = x;
    metaDataYPos = y;
    
    Rectangle <int> metaDataPosition = metaDataScreen.getBoundsInParent();
    metaDataPosition.setY (metaDataYPos);
    metaDataScreen.setBounds (metaDataPosition);
}
