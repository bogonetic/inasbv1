#ifndef SAFE_JUCE_MODULE
#define SAFE_JUCE_MODULE

#include "../juce_core/juce_core.h"
#include "../juce_audio_processors/juce_audio_processors.h"

#include "xtract/libxtract.h"

//=============================================================================
namespace juce
{
#include "LookAndFeel/SAFEButton.h"
#include "LookAndFeel/SAFESlider.h"
#include "LookAndFeel/SAFELookAndFeel.h"

#include "PluginUtils/SAFEMetaData.h"
#include "PluginUtils/SAFEMetaDataScreen.h"
#include "PluginUtils/SAFEFeatureExtractor.h"
#include "PluginUtils/SAFEParameter.h"
#include "PluginUtils/SAFEAudioProcessor.h"
#include "PluginUtils/SAFEAudioProcessorEditor.h"

#include "Filters/BrechtsIIRFilter.h"
#include "Filters/Resampler.h"
}

#endif   // SAFE_JUCE_MODULE
