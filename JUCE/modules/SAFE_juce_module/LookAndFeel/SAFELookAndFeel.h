#ifndef __SAFELOOKANDFEEL__
#define __SAFELOOKANDFEEL__

class SAFELookAndFeel : public LookAndFeel_V3
{
public:
    SAFELookAndFeel();
    ~SAFELookAndFeel();

    void drawRotarySlider (Graphics &g, int x, int y, int width, int height, float sliderPosProportional, float rotaryStartAngle, float rotaryEndAngle, Slider &slider);

    void fillTextEditorBackground (Graphics &g, int width, int height, TextEditor &textEditor);
    void drawTextEditorOutline (Graphics &g, int width, int height, TextEditor &textEditor);

private:
    Image greenSliderImage, redSliderImage, yellowSliderImage;
};

#endif // __SAFELOOKANDFEEL__
