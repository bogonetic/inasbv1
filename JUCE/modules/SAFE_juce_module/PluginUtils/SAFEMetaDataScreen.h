#ifndef __SAFEMETADATASCREEN__
#define __SAFEMETADATASCREEN__

class SAFEMetaDataScreen : public Component
{
public:
    //==========================================================================
    //      Constructor and Destructor
    //==========================================================================
    SAFEMetaDataScreen();
    ~SAFEMetaDataScreen();

    //==========================================================================
    //      Paint the Thing
    //==========================================================================
    void paint (Graphics& g);

    //==========================================================================
    //      Get the Data
    //==========================================================================
    SAFEMetaData getMetaData();

    TextButton submitButton;

private:
    TextEditor genreBox, experienceBox, locationBox, instrumentBox;
};

#endif // __SAFEMETADATASCREEN__

