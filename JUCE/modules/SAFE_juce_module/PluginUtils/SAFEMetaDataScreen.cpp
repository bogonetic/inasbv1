//==========================================================================
//      Constructor and Destructor
//==========================================================================
SAFEMetaDataScreen::SAFEMetaDataScreen()
    : submitButton ("Submit")
{
    setSize (250, 100);

    addAndMakeVisible (&genreBox);
    genreBox.setBounds (25, 20, 50, 20);

    addAndMakeVisible (&instrumentBox);
    instrumentBox.setBounds (25, 60, 50, 20);

    addAndMakeVisible (&experienceBox);
    experienceBox.setBounds (100, 20, 50, 20);

    addAndMakeVisible (&locationBox);
    locationBox.setBounds (100, 60, 50, 20);

    addAndMakeVisible (&submitButton);
    submitButton.setBounds (175, 60, 50, 20);
}

SAFEMetaDataScreen::~SAFEMetaDataScreen()
{
}

//==========================================================================
//      Paint the Thing
//==========================================================================
void SAFEMetaDataScreen::paint (Graphics& g)
{
    g.fillAll (Colours::green);
}

//==========================================================================
//      Get the Data
//==========================================================================
SAFEMetaData SAFEMetaDataScreen::getMetaData()
{
    SAFEMetaData metaData;

    metaData.genre = genreBox.getText();
    metaData.instrument = instrumentBox.getText();
    metaData.experience = experienceBox.getText();
    metaData.location = locationBox.getText();

    return metaData;
}
