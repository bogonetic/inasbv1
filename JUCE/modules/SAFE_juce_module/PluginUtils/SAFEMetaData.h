#ifndef __SAFEMETADATA__
#define __SAFEMETADATA__

struct SAFEMetaData
{
    String genre, instrument, location, experience;
};

#endif // __SAFEMETADATA__
