#ifndef __SAFEAUDIOPROCESSOREDITOR__
#define __SAFEAUDIOPROCESSOREDITOR__

class SAFEAudioProcessorEditor : public AudioProcessorEditor,
                                 public Button::Listener,
                                 public Slider::Listener,
                                 public Timer
{
public:
    //==========================================================================
    //      Constructor and Destructor
    //==========================================================================
    SAFEAudioProcessorEditor (SAFEAudioProcessor* ownerFilter);
    ~SAFEAudioProcessorEditor();

    //==========================================================================
    //      User Interaction Listeners
    //==========================================================================
    void buttonClicked (Button* button);
    void sliderValueChanged (Slider* slider);

    //==========================================================================
    //      GUI Update Timer
    //==========================================================================
    void timerCallback();

protected:
    TextEditor descriptorBox;
    SAFEButton recordButton, loadButton;
    OwnedArray <SAFESlider> sliders;

    //==========================================================================
    //      Meta Data Screen Bits
    //==========================================================================
    SAFEButton metaDataButton;
    void setMetaDataScreenPosition (int x, int y);

private:
    ComponentAnimator animator;

    SAFEMetaDataScreen metaDataScreen;
    int metaDataXPos, metaDataYPos;

    int numParameters;
    const OwnedArray <SAFEParameter>& parameters;

    ScopedPointer <SAFELookAndFeel> lookAndFeel;

    SAFEAudioProcessor* getProcessor()
    {
        return static_cast <SAFEAudioProcessor*> (getAudioProcessor());
    }
};

#endif // __SAFEAUDIOPROCESSOREDITOR__
