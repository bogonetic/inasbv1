#ifndef __SAFEFEATUREEXTRACTOR__
#define __SAFEFEATUREEXTRACTOR__

class SAFEFeatureExtractor
{
public:
    //==========================================================================
    //      Constructor and Destructor
    //==========================================================================
    SAFEFeatureExtractor();
    ~SAFEFeatureExtractor();

    void initialise (int numAnalysisFramesInit, int analysisFrameLengthInit, double sampleRate);

    void getAllFeatures (double* sampleData, int numSamples, int frameNum);

    void addToXml (XmlElement* parentElement);

private:
    HeapBlock <double> means;
    HeapBlock <double> variances;
    HeapBlock <double> standardDeviations;
    HeapBlock <double> rmsAmplitudes;
    HeapBlock <double> zeroCrossingRates;

    HeapBlock <double> spectrum;
    HeapBlock <double> spectralCentroids;
    HeapBlock <double> spectralVariances;
    HeapBlock <double> spectralStandardDeviations;
    HeapBlock <double> spectralSkewnesss;
    HeapBlock <double> spectralKurtosiss;
    HeapBlock <double> irregularityJs;
    HeapBlock <double> irregularityKs;
    HeapBlock <double> fundamentals;
    HeapBlock <double> smoothnesss;
    HeapBlock <double> rolloffs;
    HeapBlock <double> flatnesss;
    HeapBlock <double> tonalities;
    HeapBlock <double> crests;
    HeapBlock <double> spectralSlopes;

    HeapBlock <double> peakSpectrum;
    HeapBlock <double> peakSpectralCentroids;
    HeapBlock <double> peakSpectralVariances;
    HeapBlock <double> peakSpectralStandardDeviations;
    HeapBlock <double> peakSpectralSkewnesss;
    HeapBlock <double> peakSpectralKurtosiss;
    HeapBlock <double> peakIrregularityJs;
    HeapBlock <double> peakIrregularityKs;
    HeapBlock <double> peakTristimulus1s;
    HeapBlock <double> peakTristimulus2s;
    HeapBlock <double> peakTristimulus3s;
    HeapBlock <double> inharmonicities;

    HeapBlock <double> harmonicSpectrum;
    HeapBlock <double> harmonicSpectralCentroids;
    HeapBlock <double> harmonicSpectralVariances;
    HeapBlock <double> harmonicSpectralStandardDeviations;
    HeapBlock <double> harmonicSpectralSkewnesss;
    HeapBlock <double> harmonicSpectralKurtosiss;
    HeapBlock <double> harmonicIrregularityJs;
    HeapBlock <double> harmonicIrregularityKs;
    HeapBlock <double> harmonicTristimulus1s;
    HeapBlock <double> harmonicTristimulus2s;
    HeapBlock <double> harmonicTristimulus3s;
    HeapBlock <double> noisinesss;
    HeapBlock <double> parityRatios;

    HeapBlock <int> barkBandLimits;
    HeapBlock <double> barkCoefficients;
    HeapBlock <double> loudnesss;
    HeapBlock <double> sharpnesss;

    xtract_mel_filter melFilters;
    HeapBlock <double> mfccs;
    HeapBlock <double> mfccMeans;
    HeapBlock <double> mfccVariances;
    HeapBlock <double> mfccStandardDeviations;
    HeapBlock <double> mfccSkewnesss;
    HeapBlock <double> mfccKurtosiss;

    int numAnalysisFrames;
    int analysisFrameLength;
    double fs;

    bool initialised;
};

#endif //__SAFEFEATUREEXTRACTOR__
