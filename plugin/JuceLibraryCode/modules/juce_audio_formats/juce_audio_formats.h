// This is an auto-generated file to redirect any included
// module headers to the correct external folder.

#if defined (JUCER_XCODE_MAC_F6D2F4CF)
 #include "../../../../juce/modules/juce_audio_formats/juce_audio_formats.h"
#elif defined (JUCER_VS2012_78A501F)
 #include "../../../../juce/modules/juce_audio_formats/juce_audio_formats.h"
#elif defined (JUCER_LINUX_MAKE_7346DA2A)
 #include "../../../../JUCE/modules/juce_audio_formats/juce_audio_formats.h"
#else
 #error "This file is designed to be used in an Introjucer-generated project!"
#endif

