String makeXmlString (String input)
{
    return input.retainCharacters ("1234567890qwertyuioplkjhgfdsazxcvbnmMNBVCXZASDFGHJKLPOIUYTREWQ:-_");
}

//==========================================================================
//      Constructor and Destructor
//==========================================================================
SAFEAudioProcessor::SAFEAudioProcessor()
{
    unprocessedTap = processedTap = 0;

    initialiseSemanticDataFile();

    playHead.resetToDefault();

    recording = false;
    readyToSave = false;

    xtract_init_fft (analysisFrameLength, XTRACT_SPECTRUM);

    numInputs = 1;
    numOutputs = 1;
}

SAFEAudioProcessor::~SAFEAudioProcessor()
{
    xtract_free_fft();
}

//==========================================================================
//      Parameter Info Methods
//==========================================================================
const String SAFEAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

int SAFEAudioProcessor::getNumParameters()
{
    return parameters.size();
}

float SAFEAudioProcessor::getParameter (int index)
{
    return parameters [index]->getBaseValue();
}

void SAFEAudioProcessor::setParameter (int index, float newValue)
{
    parameters [index]->setBaseValue (newValue);
    
    parameterUpdateCalculations (index);
}

float SAFEAudioProcessor::getScaledParameter (int index)
{
    return parameters [index]->getScaledValue();
}

void SAFEAudioProcessor::setScaledParameter (int index, float newValue)
{
    parameters [index]->setScaledValue (newValue);
    
    parameterUpdateCalculations (index);
}

void SAFEAudioProcessor::setScaledParameterNotifyingHost (int index, float newValue)
{
    setScaledParameter (index, newValue);
    float newBaseValue = parameters [index]->getBaseValue();
    sendParamChangeMessageToListeners (index, newBaseValue);
}

const String SAFEAudioProcessor::getParameterName (int index)
{
    return parameters [index]->getName();
}

const String SAFEAudioProcessor::getParameterText (int index)
{
    SAFEParameter* info = parameters [index];
    return String (info->getScaledValue(), 2) + info->getUnits();
}

const OwnedArray <SAFEParameter>& SAFEAudioProcessor::getParameterArray()
{
    return parameters;
}

//==========================================================================
//      Other Plugin Info
//==========================================================================
const String SAFEAudioProcessor::getInputChannelName (int channelIndex) const
{
    return String (channelIndex + 1);
}

const String SAFEAudioProcessor::getOutputChannelName (int channelIndex) const
{
    return String (channelIndex + 1);
}

bool SAFEAudioProcessor::isInputChannelStereoPair (int index) const
{
    return true;
}

bool SAFEAudioProcessor::isOutputChannelStereoPair (int index) const
{
    return true;
}

bool SAFEAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool SAFEAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool SAFEAudioProcessor::silenceInProducesSilenceOut() const
{
    return false;
}

double SAFEAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

//==========================================================================
//      Program Stuff
//==========================================================================
int SAFEAudioProcessor::getNumPrograms()
{
    return 0;
}

int SAFEAudioProcessor::getCurrentProgram()
{
    return 0;
}

void SAFEAudioProcessor::setCurrentProgram (int index)
{
}

const String SAFEAudioProcessor::getProgramName (int index)
{
    return String::empty;
}

void SAFEAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==========================================================================
//      Saving and Loading Patches
//==========================================================================
void SAFEAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    XmlElement xml (makeXmlString(JucePlugin_Name + String ("Settings")));

    for (int parameterNum = 0; parameterNum < parameters.size(); ++parameterNum)
    {
        xml.setAttribute ("Parameter" + String (parameterNum), parameters [parameterNum]->getBaseValue());
    }

    copyXmlToBinary (xml, destData);
}

void SAFEAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    ScopedPointer<XmlElement> xmlState (getXmlFromBinary (data, sizeInBytes));

    if (xmlState != nullptr)
    {
        if (xmlState->hasTagName (makeXmlString (JucePlugin_Name + String ("Settings"))))
        {
            for (int parameterNum = 0; parameterNum < parameters.size(); ++parameterNum)
            {
                setParameterNotifyingHost (parameterNum, (float) xmlState->getDoubleAttribute ("Parameter" + String (parameterNum), parameters [parameterNum]->getBaseValue()));
            }
        }
    }
}

//==========================================================================
//      Semantic Data Parsing
//==========================================================================
void SAFEAudioProcessor::initialiseSemanticDataFile()
{
    File desktopDirectory (File::getSpecialLocation (File::userDocumentsDirectory));

    File dataDirectory (desktopDirectory.getChildFile ("SAFEPluginData"));

    if (! dataDirectory.isDirectory())
    {
        dataDirectory.createDirectory();
    }

    semanticDataFile = dataDirectory.getChildFile (JucePlugin_Name + String ("Data.xml"));

    if (semanticDataFile.exists())
    {
        XmlDocument semanticDataDocument (semanticDataFile);
        semanticDataElement = semanticDataDocument.getDocumentElement();
    }
    else
    {
        String elementName (JucePlugin_Name + String ("Data"));
        semanticDataElement = new XmlElement (makeXmlString (elementName));
    }
}

void SAFEAudioProcessor::saveSemanticData (String newDescriptors, SAFEMetaData& metaData)
{
    // separate different descriptors
    StringArray descriptors;
    descriptors.addTokens (newDescriptors, true);
    int numDescriptors = descriptors.size();

    XmlElement* descriptorElement = semanticDataElement->createNewChildElement ("SemanticData");

    for (int descriptor = 0; descriptor < numDescriptors; ++descriptor)
    {
        String descriptorString = descriptors [descriptor];
        String descriptorName = String ("Descriptor") + String (descriptor);

        descriptorElement->setAttribute (descriptorName, descriptorString);
    }
    
    // save the channel configuration
    XmlElement* configElement = descriptorElement->getChildByName ("ChannelConfiguration");

    if (! configElement)
    {
        configElement = descriptorElement->createNewChildElement ("ChannelConfiguration");
    }

    configElement->setAttribute ("Inputs", numInputs);
    configElement->setAttribute ("Outputs", numOutputs);

    // save the parameter settings
    XmlElement* parametersElement = descriptorElement->getChildByName ("ParameterSettings");

    if (! parametersElement)
    {
        parametersElement = descriptorElement->createNewChildElement ("ParameterSettings");
    }

    for (int parameterNum = 0; parameterNum < parameters.size(); ++parameterNum)
    {
        SAFEParameter* currentParameter = parameters [parameterNum];
        String xmlParameterName = makeXmlString (currentParameter->getName());
        float currentParameterValue = currentParameter->getScaledValue();

        parametersElement->setAttribute (xmlParameterName, currentParameterValue);
    }

    // save the unprocessed audio features
    XmlElement* unprocessedFeaturesElement = descriptorElement->getChildByName ("UnprocessedAudioFeatures");

    if (! unprocessedFeaturesElement)
    {
        unprocessedFeaturesElement = descriptorElement->createNewChildElement ("UnprocessedAudioFeatures");
    }
    
    for (int inputChannel = 0; inputChannel < numInputs; ++inputChannel)
    {
        String channelName = String ("Channel") + String (inputChannel);

        XmlElement* unprocessedChannelElement = unprocessedFeaturesElement->getChildByName (channelName);

        if (! unprocessedChannelElement)
        {
            unprocessedChannelElement = unprocessedFeaturesElement->createNewChildElement (channelName);
        }

        unprocessedFeatureExtractors [inputChannel]->addToXml (unprocessedChannelElement);
    }

    //save the processed audio features
    XmlElement* processedFeaturesElement = descriptorElement->getChildByName ("ProcessedAudioFeatures");

    if (! processedFeaturesElement)
    {
        processedFeaturesElement = descriptorElement->createNewChildElement ("ProcessedAudioFeatures");
    }

    for (int outputChannel = 0; outputChannel < numInputs; ++outputChannel)
    {
        String channelName = String ("Channel") + String (outputChannel);

        XmlElement* processedChannelElement = processedFeaturesElement->getChildByName (channelName);

        if (! processedChannelElement)
        {
            processedChannelElement = processedFeaturesElement->createNewChildElement (channelName);
        }

        processedFeatureExtractors [outputChannel]->addToXml (processedChannelElement);
    }

    //save the meta data
    XmlElement* metaDataElement = descriptorElement->getChildByName ("MetaData");

    if (! metaDataElement)
    {
        metaDataElement = descriptorElement->createNewChildElement ("MetaData");
    }

    metaDataElement->setAttribute ("Genre", metaData.genre);
    metaDataElement->setAttribute ("Instrument", metaData.instrument);
    metaDataElement->setAttribute ("Location", metaData.location);
    metaDataElement->setAttribute ("Experience", metaData.experience);

    // save to file
    semanticDataElement->writeToFile (semanticDataFile, "");

    readyToSave = false;
}

void SAFEAudioProcessor::loadSemanticData (String descriptor)
{
    String xmlDescriptor = makeXmlString (descriptor);

    XmlElement* descriptorElement = semanticDataElement->getChildByName (xmlDescriptor);

    if (descriptorElement)
    {
        XmlElement* parametersElement = descriptorElement->getChildByName ("ParameterSettings");

        for (int parameterNum = 0; parameterNum < parameters.size(); ++parameterNum)
        {
            SAFEParameter* currentParameter = parameters [parameterNum];
            String xmlParameterName = makeXmlString (currentParameter->getName());
            float newParameterValue = (float) parametersElement->getDoubleAttribute (xmlParameterName);
            
            setScaledParameterNotifyingHost (parameterNum, newParameterValue);
        }
    }
}

//==========================================================================
//      Process Block
//==========================================================================
void SAFEAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    numInputs = getNumInputChannels();
    numOutputs = getNumOutputChannels();

    int samplesInRecording = floor (sampleRate * analysisTime / 1000);
    numAnalysisFrames = floor (samplesInRecording / analysisFrameLength);

    unprocessedBuffer.clear();
    unprocessedFeatureExtractors.clear();

    for (int inputChannel = 0; inputChannel < numInputs; ++inputChannel)
    {
        unprocessedBuffer.add (new Array <double>);
        unprocessedBuffer [inputChannel]->resize (analysisFrameLength);

        unprocessedFeatureExtractors.add (new SAFEFeatureExtractor);
        unprocessedFeatureExtractors [inputChannel]->initialise (numAnalysisFrames, analysisFrameLength, sampleRate);
    }

    processedBuffer.clear();
    processedFeatureExtractors.clear();

    for (int outputChannel = 0; outputChannel < numOutputs; ++outputChannel)
    {
        processedBuffer.add (new Array <double>);
        processedBuffer [outputChannel]->resize (analysisFrameLength);

        processedFeatureExtractors.add (new SAFEFeatureExtractor);
        processedFeatureExtractors [outputChannel]->initialise (numAnalysisFrames, analysisFrameLength, sampleRate);
    }
    
    pluginPreparation (sampleRate, samplesPerBlock);
}

void SAFEAudioProcessor::processBlock (AudioSampleBuffer& buffer, MidiBuffer& midiMessages)
{
    localRecording = recording;

    analyseUnprocessedSamples (buffer);

    pluginProcessing (buffer, midiMessages);

    // In case we have more outputs than inputs, we'll clear any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    for (int i = getNumInputChannels(); i < getNumOutputChannels(); ++i)
    {
        buffer.clear (i, 0, buffer.getNumSamples());
    }

    updatePlayHead();
    analyseProcessedSamples (buffer);    
}

//==========================================================================
//      Playing & Recording Info
//==========================================================================
bool SAFEAudioProcessor::isPlaying()
{
    return playHead.isPlaying;
}

void SAFEAudioProcessor::startRecording()
{
    currentUnprocessedAnalysisFrame = 0;
    currentProcessedAnalysisFrame = 0;
    unprocessedTap = 0;
    processedTap = 0;

    recording = true;
}

bool SAFEAudioProcessor::isRecording()
{
    return recording;
}

bool SAFEAudioProcessor::isReadyToSave()
{
    return readyToSave;
}

//==========================================================================
//      Methods to Create New Parameters
//==========================================================================
void SAFEAudioProcessor::addParameter (String name, float& valueRef, float initialValue, float minValue, float maxValue, String units, float skewFactor)
{
    parameters.add (new SAFEParameter (name, valueRef, initialValue, minValue, maxValue, units, skewFactor));
}

//==========================================================================
//      Buffer Playing Audio For Analysis
//==========================================================================
void SAFEAudioProcessor::analyseUnprocessedSamples (AudioSampleBuffer& buffer)
{
    if (localRecording)
    {
        int numSamples = buffer.getNumSamples();

        for (int i = 0; i < numSamples; ++i)
        {
            for (int inputChannel = 0; inputChannel < numInputs; ++inputChannel)
            {
                unprocessedBuffer [inputChannel]->set (unprocessedTap, buffer.getSample (inputChannel, i));
            }
            
            ++unprocessedTap;

            if (unprocessedTap >= analysisFrameLength)
            {
                unprocessedTap %= analysisFrameLength;

                for (int inputChannel = 0; inputChannel < numInputs; ++inputChannel)
                {
                    double* unprocessedSamples = unprocessedBuffer.getRawDataPointer() [inputChannel]->getRawDataPointer();

                    unprocessedFeatureExtractors [inputChannel]->getAllFeatures (unprocessedSamples, analysisFrameLength, currentUnprocessedAnalysisFrame);
                }

                ++currentUnprocessedAnalysisFrame;

                if (currentUnprocessedAnalysisFrame >= numAnalysisFrames)
                {
                    break;
                }
            }
        }
    }
}

void SAFEAudioProcessor::analyseProcessedSamples (AudioSampleBuffer& buffer)
{
    if (localRecording)
    {
        int numSamples = buffer.getNumSamples();

        for (int i = 0; i < numSamples; ++i)
        {
            for (int outputChannel = 0; outputChannel < numOutputs; ++outputChannel)
            {
                processedBuffer [outputChannel]->set (processedTap, buffer.getSample (outputChannel, i));
            }
            
            ++processedTap;

            if (processedTap >= analysisFrameLength)
            {
                processedTap %= analysisFrameLength;

                for (int outputChannel = 0; outputChannel < numOutputs; ++outputChannel)
                {
                    double* processedSamples = processedBuffer.getRawDataPointer() [outputChannel]->getRawDataPointer();

                    processedFeatureExtractors [outputChannel]->getAllFeatures (processedSamples, analysisFrameLength, currentProcessedAnalysisFrame);
                }

                ++currentProcessedAnalysisFrame;

                if (currentProcessedAnalysisFrame >= numAnalysisFrames)
                {
                    recording = false;
                    readyToSave = true;
                    break;
                }
            }
        }
    }
}

//==========================================================================
//      Play Head Stuff
//==========================================================================
void SAFEAudioProcessor::updatePlayHead()
{
    AudioPlayHead::CurrentPositionInfo newPlayHead;
    
    if (getPlayHead() != nullptr && getPlayHead()->getCurrentPosition (newPlayHead))
    {
        playHead = newPlayHead;
    }
    else
    {
        playHead.resetToDefault();
    }
}
